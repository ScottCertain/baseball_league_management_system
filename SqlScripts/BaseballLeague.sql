USE [master]
GO
/****** Object:  Database [BaseballLeague]    Script Date: 2/23/2016 3:29:55 PM ******/
CREATE DATABASE [BaseballLeague]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BaseballLeague', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BaseballLeague.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BaseballLeague_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BaseballLeague_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BaseballLeague] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BaseballLeague].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BaseballLeague] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BaseballLeague] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BaseballLeague] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BaseballLeague] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BaseballLeague] SET ARITHABORT OFF 
GO
ALTER DATABASE [BaseballLeague] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BaseballLeague] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BaseballLeague] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BaseballLeague] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BaseballLeague] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BaseballLeague] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BaseballLeague] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BaseballLeague] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BaseballLeague] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BaseballLeague] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BaseballLeague] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BaseballLeague] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BaseballLeague] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BaseballLeague] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BaseballLeague] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BaseballLeague] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BaseballLeague] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BaseballLeague] SET RECOVERY FULL 
GO
ALTER DATABASE [BaseballLeague] SET  MULTI_USER 
GO
ALTER DATABASE [BaseballLeague] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BaseballLeague] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BaseballLeague] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BaseballLeague] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BaseballLeague] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BaseballLeague', N'ON'
GO
USE [BaseballLeague]
GO
/****** Object:  Table [dbo].[Games]    Script Date: 2/23/2016 3:29:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Games](
	[GameId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[HomeTeamId] [int] NOT NULL,
	[AwayTeamId] [int] NOT NULL,
	[HomeScore] [int] NULL,
	[AwayScore] [int] NULL,
 CONSTRAINT [PK_Games] PRIMARY KEY CLUSTERED 
(
	[GameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Leagues]    Script Date: 2/23/2016 3:29:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Leagues](
	[LeagueId] [int] IDENTITY(1,1) NOT NULL,
	[LeagueName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Leagues] PRIMARY KEY CLUSTERED 
(
	[LeagueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Managers]    Script Date: 2/23/2016 3:29:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Managers](
	[ManagerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Managers] PRIMARY KEY CLUSTERED 
(
	[ManagerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Players]    Script Date: 2/23/2016 3:29:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Players](
	[PlayerId] [int] IDENTITY(1,1) NOT NULL,
	[TeamId] [int] NOT NULL,
	[PositionId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[JerseyNumber] [int] NOT NULL,
	[RookieYear] [int] NOT NULL,
	[LastSeasonBatAvg] [decimal](4, 3) NULL,
 CONSTRAINT [PK_Players] PRIMARY KEY CLUSTERED 
(
	[PlayerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Positions]    Script Date: 2/23/2016 3:29:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Positions](
	[PositionId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Abbreviation] [nvarchar](5) NOT NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Teams]    Script Date: 2/23/2016 3:29:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[TeamId] [int] IDENTITY(1,1) NOT NULL,
	[LeagueId] [int] NOT NULL,
	[ManagerId] [int] NOT NULL,
	[TeamName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Games] ON 

INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (1, CAST(N'2016-04-01' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (2, CAST(N'2016-04-01' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (3, CAST(N'2016-04-01' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (4, CAST(N'2016-04-02' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (5, CAST(N'2016-04-02' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (6, CAST(N'2016-04-02' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (7, CAST(N'2016-04-03' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (8, CAST(N'2016-04-03' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (9, CAST(N'2016-04-03' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (10, CAST(N'2016-04-08' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (11, CAST(N'2016-04-08' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (12, CAST(N'2016-04-08' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (13, CAST(N'2016-04-09' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (14, CAST(N'2016-04-09' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (15, CAST(N'2016-04-09' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (16, CAST(N'2016-04-10' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (17, CAST(N'2016-04-10' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (18, CAST(N'2016-04-10' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (19, CAST(N'2016-04-15' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (20, CAST(N'2016-04-15' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (21, CAST(N'2016-04-15' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (22, CAST(N'2016-04-16' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (23, CAST(N'2016-04-16' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (24, CAST(N'2016-04-16' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (25, CAST(N'2016-04-17' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (26, CAST(N'2016-04-17' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (27, CAST(N'2016-04-17' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (28, CAST(N'2016-04-22' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (29, CAST(N'2016-04-22' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (30, CAST(N'2016-04-22' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (31, CAST(N'2016-04-23' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (32, CAST(N'2016-04-23' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (33, CAST(N'2016-04-23' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (34, CAST(N'2016-04-24' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (35, CAST(N'2016-04-24' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (36, CAST(N'2016-04-24' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (37, CAST(N'2016-04-29' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (38, CAST(N'2016-04-29' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (39, CAST(N'2016-04-29' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (40, CAST(N'2016-04-30' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (41, CAST(N'2016-04-30' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (42, CAST(N'2016-04-30' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (43, CAST(N'2016-05-01' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (44, CAST(N'2016-05-01' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (45, CAST(N'2016-05-01' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (46, CAST(N'2016-05-06' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (47, CAST(N'2016-05-06' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (48, CAST(N'2016-05-06' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (49, CAST(N'2016-05-07' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (50, CAST(N'2016-05-07' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (51, CAST(N'2016-05-07' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (52, CAST(N'2016-05-08' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (53, CAST(N'2016-05-08' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (54, CAST(N'2016-05-08' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (55, CAST(N'2016-05-13' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (56, CAST(N'2016-05-13' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (57, CAST(N'2016-05-13' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (58, CAST(N'2016-05-14' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (59, CAST(N'2016-05-14' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (60, CAST(N'2016-05-14' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (61, CAST(N'2016-05-15' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (62, CAST(N'2016-05-15' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (63, CAST(N'2016-05-15' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (64, CAST(N'2016-05-20' AS Date), 8, 5, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (65, CAST(N'2016-05-20' AS Date), 1, 6, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (66, CAST(N'2016-05-20' AS Date), 2, 7, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (67, CAST(N'2016-05-21' AS Date), 3, 8, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (68, CAST(N'2016-05-21' AS Date), 4, 1, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (69, CAST(N'2016-05-21' AS Date), 5, 2, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (70, CAST(N'2016-05-22' AS Date), 6, 3, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (71, CAST(N'2016-05-22' AS Date), 7, 4, NULL, NULL)
INSERT [dbo].[Games] ([GameId], [Date], [HomeTeamId], [AwayTeamId], [HomeScore], [AwayScore]) VALUES (72, CAST(N'2016-05-22' AS Date), 8, 5, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Games] OFF
SET IDENTITY_INSERT [dbo].[Leagues] ON 

INSERT [dbo].[Leagues] ([LeagueId], [LeagueName]) VALUES (1, N'Ohio Baseball League')
SET IDENTITY_INSERT [dbo].[Leagues] OFF
SET IDENTITY_INSERT [dbo].[Managers] ON 

INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (1, N'Lewis', N'Johnson')
INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (2, N'Ben', N'Larson')
INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (3, N'Bird', N'Hunter')
INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (4, N'Frank', N'Sampson')
INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (5, N'George', N'Wilson')
INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (6, N'Tom', N'Alvarez')
INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (7, N'Bill', N'Rodrigues')
INSERT [dbo].[Managers] ([ManagerId], [FirstName], [LastName]) VALUES (8, N'Tim', N'Lee')
SET IDENTITY_INSERT [dbo].[Managers] OFF
SET IDENTITY_INSERT [dbo].[Players] ON 

INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (1, 1, 1, N'Jorge', N'Alfaro', 0, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (2, 2, 1, N'Eliezer', N'Alfonzo', 2, 2014, CAST(0.250 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (3, 3, 1, N'Bryan', N'Anderson', 5, 2013, CAST(0.315 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (4, 4, 1, N'J.P.', N'Arencibia', 8, 2012, CAST(0.630 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (5, 5, 1, N'Nevin', N'Ashley', 10, 2010, CAST(0.551 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (6, 6, 1, N'Alex', N'Avila', 15, 2013, CAST(0.470 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (7, 7, 1, N'John', N'Baker', 20, 2014, CAST(0.283 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (8, 8, 1, N'Jett', N'Bandy', 30, 2015, CAST(0.390 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (9, 1, 2, N'David', N'Aardsma', 1, 2011, CAST(0.665 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (10, 2, 2, N'Fernando', N'Abad', 4, 2013, CAST(0.510 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (11, 3, 2, N'Andury', N'Acevedo', 7, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (12, 4, 2, N'Alfredo', N'Aceves', 11, 2006, CAST(0.130 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (13, 5, 2, N'A.J.', N'Achter', 21, 2014, CAST(0.328 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (14, 6, 2, N'Manny', N'Acosta', 31, 2013, CAST(0.650 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (15, 7, 2, N'Austin', N'Adams', 42, 2012, CAST(0.589 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (16, 8, 2, N'Mike', N'Adams', 51, 2015, CAST(0.470 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (17, 1, 3, N'Jose', N'Abreu', 3, 2011, CAST(0.146 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (18, 2, 3, N'Matt', N'Adams', 6, 2013, CAST(0.210 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (19, 3, 3, N'Jesus', N'Aguilar', 9, 2014, CAST(0.699 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (20, 4, 3, N'Brandon', N'Allen', 13, 2010, CAST(0.550 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (21, 5, 3, N'Yonder', N'Alonso', 16, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (22, 6, 3, N'Pedro', N'Alvarez', 22, 2011, CAST(0.130 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (23, 7, 3, N'Lars', N'Anderson', 33, 2013, CAST(0.251 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (24, 8, 3, N'Jeff', N'Baker', 44, 2015, CAST(0.370 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (25, 1, 4, N'Tony', N'Abreu', 1, 2014, CAST(0.583 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (26, 2, 4, N'Arismendy', N'Alcantara', 2, 2012, CAST(0.490 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (27, 3, 4, N'Jose', N'Altuve', 3, 2007, CAST(0.165 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (28, 4, 4, N'Alfredo', N'Amezaga', 24, 2011, CAST(0.210 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (29, 5, 4, N'Darwin', N'Barney', 35, 2013, CAST(0.367 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (30, 6, 4, N'Tim', N'Beckham', 46, 2014, CAST(0.630 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (31, 7, 4, N'Ronnie', N'Belliard', 57, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (32, 8, 4, N'Andres', N'Blanco', 68, 2015, CAST(0.450 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (33, 1, 5, N'Cristhian', N'Adames', 5, 2010, CAST(0.289 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (34, 2, 5, N'Ehire', N'Adrianza', 6, 2011, CAST(0.570 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (35, 3, 5, N'Nick', N'Ahmed', 99, 2013, CAST(0.346 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (36, 4, 5, N'Hanser', N'Alberto', 82, 2014, CAST(0.610 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (37, 5, 5, N'Alexi', N'Amarista', 73, 2012, CAST(0.199 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (38, 6, 5, N'Elvis', N'Andrus', 45, 2008, CAST(0.550 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (39, 7, 5, N'Dean', N'Anna', 51, 2013, CAST(0.315 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (40, 8, 5, N'Orlando', N'Arcia', 63, 2015, CAST(0.630 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (41, 1, 6, N'David', N'Adams', 7, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (42, 2, 6, N'Nolan', N'Arenado', 8, 2014, CAST(0.470 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (43, 3, 6, N'Joaquin', N'Arias', 9, 2013, CAST(0.183 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (44, 4, 6, N'Gordon', N'Beckham', 62, 2011, CAST(0.290 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (45, 5, 6, N'Josh', N'Bell', 51, 2010, CAST(0.365 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (46, 6, 6, N'Adrian', N'Beltre', 43, 2013, CAST(0.610 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (47, 7, 6, N'Reid', N'Brignac', 39, 2014, CAST(0.567 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (48, 8, 6, N'Kris', N'Bryant', 28, 2015, CAST(0.430 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (49, 1, 7, N'Dustin', N'Ackley', 17, 2012, CAST(0.228 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (50, 2, 7, N'Zoilo', N'Almonte', 2, 2013, CAST(0.350 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (51, 3, 7, N'Moises', N'Alou', 20, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (52, 4, 7, N'Nori', N'Aoki', 45, 2011, CAST(0.570 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (53, 5, 7, N'Oswaldo', N'Arcia', 85, 2014, CAST(0.446 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (54, 6, 7, N'Cody', N'Asche', 65, 2013, CAST(0.110 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (55, 7, 7, N'Xavier', N'Avery', 35, 2009, CAST(0.399 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (56, 8, 7, N'Brandon', N'Barnes', 79, 2015, CAST(0.650 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (57, 1, 8, N'Lane', N'Adams', 74, 2011, CAST(0.515 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (58, 2, 8, N'Abraham', N'Almonte', 75, 2013, CAST(0.430 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (59, 3, 8, N'Aaron', N'Altherr', 78, 2014, CAST(0.151 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (60, 4, 8, N'Matt', N'Angle', 36, 2012, CAST(0.270 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (61, 5, 8, N'Joe', N'Benson', 35, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (62, 6, 8, N'Wynton', N'Bernard', 32, 2013, CAST(0.590 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (63, 7, 8, N'Mookie', N'Betts', 52, 2010, CAST(0.465 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (64, 8, 8, N'Charlie', N'Blackmon', 58, 2015, CAST(0.110 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (65, 1, 9, N'Cory', N'Aldridge', 47, 2014, CAST(0.267 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (66, 2, 9, N'Dariel', N'Alvarez', 45, 2013, CAST(0.330 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (67, 3, 9, N'Andrew', N'Aplin', 42, 2011, CAST(0.528 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (68, 4, 9, N'Wladimir', N'Balentien', 21, 2012, CAST(0.450 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (69, 5, 9, N'Jose', N'Bautista', 25, 2013, CAST(0.189 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (70, 6, 9, N'Mike', N'Baxter', 23, 2014, CAST(0.270 AS Decimal(4, 3)))
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (71, 7, 9, N'Carlos', N'Beltran', 60, 2016, NULL)
INSERT [dbo].[Players] ([PlayerId], [TeamId], [PositionId], [FirstName], [LastName], [JerseyNumber], [RookieYear], [LastSeasonBatAvg]) VALUES (72, 8, 9, N'Engel', N'Beltre', 53, 2015, CAST(0.610 AS Decimal(4, 3)))
SET IDENTITY_INSERT [dbo].[Players] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (1, N'Catcher', N'C')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (2, N'Pitcher', N'P')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (3, N'First Base', N'1B')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (4, N'Second Base', N'2B')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (5, N'Shortstop', N'SS')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (6, N'Third Base', N'3B')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (7, N'Left Field', N'LF')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (8, N'Center Field', N'CF')
INSERT [dbo].[Positions] ([PositionId], [Name], [Abbreviation]) VALUES (9, N'Right Field', N'RF')
SET IDENTITY_INSERT [dbo].[Positions] OFF
SET IDENTITY_INSERT [dbo].[Teams] ON 

INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (1, 1, 1, N'Spartans')
INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (2, 1, 2, N'Kangaroos')
INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (3, 1, 3, N'Cardinals')
INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (4, 1, 4, N'Flyers')
INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (5, 1, 5, N'Cubs')
INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (6, 1, 6, N'Raiders')
INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (7, 1, 7, N'Angels')
INSERT [dbo].[Teams] ([TeamId], [LeagueId], [ManagerId], [TeamName]) VALUES (8, 1, 8, N'Bucks')
SET IDENTITY_INSERT [dbo].[Teams] OFF
ALTER TABLE [dbo].[Games]  WITH NOCHECK ADD  CONSTRAINT [FK_Games_Teams] FOREIGN KEY([HomeTeamId])
REFERENCES [dbo].[Teams] ([TeamId])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Teams]
GO
ALTER TABLE [dbo].[Games]  WITH NOCHECK ADD  CONSTRAINT [FK_Games_Teams1] FOREIGN KEY([AwayTeamId])
REFERENCES [dbo].[Teams] ([TeamId])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Teams1]
GO
ALTER TABLE [dbo].[Players]  WITH NOCHECK ADD  CONSTRAINT [FK_Players_Positions] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Positions] ([PositionId])
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_Players_Positions]
GO
ALTER TABLE [dbo].[Players]  WITH NOCHECK ADD  CONSTRAINT [FK_Players_Teams] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([TeamId])
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_Players_Teams]
GO
ALTER TABLE [dbo].[Teams]  WITH NOCHECK ADD  CONSTRAINT [FK_Teams_Leagues] FOREIGN KEY([LeagueId])
REFERENCES [dbo].[Leagues] ([LeagueId])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Leagues]
GO
ALTER TABLE [dbo].[Teams]  WITH NOCHECK ADD  CONSTRAINT [FK_Teams_Managers] FOREIGN KEY([ManagerId])
REFERENCES [dbo].[Managers] ([ManagerId])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Managers]
GO
