USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadTeamById]    Script Date: 2/26/2016 10:33:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadTeamById]
(
	@TeamId int
) AS

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName [Name]
FROM Teams t
WHERE t.TeamId = @TeamId
GO

