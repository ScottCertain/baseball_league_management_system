USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[RemovePlayer]    Script Date: 2/25/2016 12:32:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemovePlayer]
(
	@PlayerId int
) AS

DELETE
FROM Players
WHERE PlayerId = @PlayerId

GO

