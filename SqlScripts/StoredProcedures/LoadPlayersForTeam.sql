USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPlayersForTeam]    Script Date: 2/26/2016 10:34:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadPlayersForTeam]
(
	@TeamId int
) AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.TeamId = @TeamId
GO

