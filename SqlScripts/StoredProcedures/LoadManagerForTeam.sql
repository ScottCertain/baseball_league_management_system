USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadManagerForTeam]    Script Date: 2/26/2016 10:34:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadManagerForTeam]
(
	@TeamId int
) AS

SELECT m.*
FROM Teams t
	INNER JOIN Managers m
	ON t.ManagerId = m.ManagerId
WHERE t.TeamId = @TeamId
GO

