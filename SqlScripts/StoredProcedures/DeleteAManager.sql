USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[DeleteAManager]    Script Date: 2/26/2016 10:02:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteAManager]
(
	@ManagerId int
) AS
DELETE FROM Managers
WHERE ManagerId = @ManagerId
GO

