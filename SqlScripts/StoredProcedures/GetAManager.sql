USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[GetAManager]    Script Date: 2/26/2016 10:02:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAManager]
(
	@ManagerId int
) AS

SELECT * FROM Managers
WHERE ManagerId = @ManagerId
GO

