USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditAManager]    Script Date: 2/26/2016 10:02:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EditAManager]
(
	@ManagerId int,
	@FirstName nvarchar(50),
	@Lastname nvarchar(50)
) AS
UPDATE Managers 
SET FirstName = @FirstName, LastName = @LastName
WHERE ManagerId = @ManagerId

GO

