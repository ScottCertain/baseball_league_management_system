USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPlayerById]    Script Date: 2/25/2016 12:32:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadPlayerById]
(
	@PlayerId int
) AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

