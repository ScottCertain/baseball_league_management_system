USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddTeam]    Script Date: 2/26/2016 10:35:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddTeam]
(
	@LeagueId int,
	@ManagerId int,
	@TeamName nvarchar(50)
) AS 

DECLARE @id int

INSERT INTO Teams (LeagueId, ManagerId, TeamName)
VALUES (@LeagueId, @ManagerId, @TeamName)

SET @id = SCOPE_IDENTITY()

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName Name
FROM Teams t
WHERE t.TeamId = @id
GO

