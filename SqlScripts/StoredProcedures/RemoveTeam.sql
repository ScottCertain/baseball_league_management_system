USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[RemoveTeam]    Script Date: 2/26/2016 11:21:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemoveTeam]
(
	@TeamId int
) AS

DELETE
FROM Teams
WHERE TeamId = @TeamId
GO

