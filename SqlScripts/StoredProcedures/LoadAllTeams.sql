USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllTeams]    Script Date: 2/26/2016 10:35:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadAllTeams] AS

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName [Name]
FROM Teams t
GO

