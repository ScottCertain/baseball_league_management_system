USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditPlayer]    Script Date: 2/25/2016 12:32:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditPlayer]
(
	@PlayerId int,
	@TeamId int,
	@PositionId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@JerseyNumber int,
	@RookieYear int,
	@LastSeasonBatAvg decimal(4,3)
) AS

UPDATE Players
	SET TeamId = @TeamId, PositionId = @PositionId, FirstName = @FirstName, LastName = @LastName,
		JerseyNumber = @JerseyNumber, RookieYear = @RookieYear, LastSeasonBatAvg = @LastSeasonBatAvg
WHERE PlayerId = @PlayerId

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

