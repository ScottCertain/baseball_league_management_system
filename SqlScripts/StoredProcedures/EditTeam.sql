USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditTeam]    Script Date: 2/26/2016 10:35:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditTeam]
(
	@TeamId int,
	@LeagueId int,
	@ManagerId int,
	@TeamName nvarchar(50)
) AS

UPDATE Teams
	SET LeagueId = @LeagueId, ManagerId = @ManagerId, TeamName = @TeamName
WHERE TeamId = @TeamId
GO

