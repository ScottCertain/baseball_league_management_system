USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllPositions]    Script Date: 2/29/2016 3:40:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LoadAllPositions] AS

SELECT * FROM Positions
GO

