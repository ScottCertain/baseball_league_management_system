USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPositionForPlayer]    Script Date: 2/27/2016 6:07:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LoadPositionForPlayer]
(
	@PlayerId int
) AS

SELECT pos.*
	FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

