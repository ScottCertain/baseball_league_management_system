USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddPlayer]    Script Date: 2/25/2016 12:32:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddPlayer]
(
	@TeamId int,
	@PositionId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@JerseyNumber int,
	@RookieYear int,
	@LastSeasonBatAvg decimal(4,3)
) AS

DECLARE @id int

INSERT INTO Players (TeamId, PositionId, FirstName, LastName, JerseyNumber, RookieYear, LastSeasonBatAvg)
VALUES (@TeamId, @PositionId, @FirstName, @LastName, @JerseyNumber, @RookieYear, @LastSeasonBatAvg)

SET @id = SCOPE_IDENTITY()

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @id

--DECLARE @InsertedRecord table
--(	
--	PlayerId int,
--	TeamId int,
--	PositionId int,
--	FirstName nvarchar(50),
--	LastName nvarchar(50),
--	JerseyNumber int,
--	RookieYear int,
--	LastSeasonBatAvg decimal(4,3)
--)

--OUTPUT INSERTED.PlayerId, INSERTED.TeamId, INSERTED.PositionId, INSERTED.FirstName, INSERTED.LastName,
--	INSERTED.JerseyNumber, INSERTED.RookieYear, INSERTED.LastSeasonBatAvg
--	INTO @InsertedRecord
GO

