USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddAManager]    Script Date: 2/26/2016 10:02:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddAManager]
(
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@ManagerId int OUTPUT
) AS
INSERT INTO Managers (FirstName, LastName)
VALUES (@FirstName, @LastName);

SET @ManagerId = SCOPE_IDENTITY();

GO

