USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllPositions]    Script Date: 2/29/2016 3:40:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LoadAllPositions] AS

SELECT * FROM Positions
GO



USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllPositions]    Script Date: 2/29/2016 3:40:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LoadAllPositions] AS

SELECT * FROM Positions
GO



USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[RemoveTeam]    Script Date: 2/26/2016 11:21:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemoveTeam]
(
	@TeamId int
) AS

DELETE
FROM Teams
WHERE TeamId = @TeamId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[RemoveTeam]    Script Date: 2/26/2016 11:21:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemoveTeam]
(
	@TeamId int
) AS

DELETE
FROM Teams
WHERE TeamId = @TeamId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddAManager]    Script Date: 2/26/2016 10:02:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddAManager]
(
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@ManagerId int OUTPUT
) AS
INSERT INTO Managers (FirstName, LastName)
VALUES (@FirstName, @LastName);

SET @ManagerId = SCOPE_IDENTITY();

GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddPlayer]    Script Date: 2/25/2016 12:32:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddPlayer]
(
	@TeamId int,
	@PositionId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@JerseyNumber int,
	@RookieYear int,
	@LastSeasonBatAvg decimal(4,3)
) AS

DECLARE @id int

INSERT INTO Players (TeamId, PositionId, FirstName, LastName, JerseyNumber, RookieYear, LastSeasonBatAvg)
VALUES (@TeamId, @PositionId, @FirstName, @LastName, @JerseyNumber, @RookieYear, @LastSeasonBatAvg)

SET @id = SCOPE_IDENTITY()

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @id

--DECLARE @InsertedRecord table
--(	
--	PlayerId int,
--	TeamId int,
--	PositionId int,
--	FirstName nvarchar(50),
--	LastName nvarchar(50),
--	JerseyNumber int,
--	RookieYear int,
--	LastSeasonBatAvg decimal(4,3)
--)

--OUTPUT INSERTED.PlayerId, INSERTED.TeamId, INSERTED.PositionId, INSERTED.FirstName, INSERTED.LastName,
--	INSERTED.JerseyNumber, INSERTED.RookieYear, INSERTED.LastSeasonBatAvg
--	INTO @InsertedRecord
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddTeam]    Script Date: 2/26/2016 10:35:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddTeam]
(
	@LeagueId int,
	@ManagerId int,
	@TeamName nvarchar(50)
) AS 

DECLARE @id int

INSERT INTO Teams (LeagueId, ManagerId, TeamName)
VALUES (@LeagueId, @ManagerId, @TeamName)

SET @id = SCOPE_IDENTITY()

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName Name
FROM Teams t
WHERE t.TeamId = @id
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[DeleteAManager]    Script Date: 2/26/2016 10:02:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteAManager]
(
	@ManagerId int
) AS
DELETE FROM Managers
WHERE ManagerId = @ManagerId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditAManager]    Script Date: 2/26/2016 10:02:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EditAManager]
(
	@ManagerId int,
	@FirstName nvarchar(50),
	@Lastname nvarchar(50)
) AS
UPDATE Managers 
SET FirstName = @FirstName, LastName = @LastName
WHERE ManagerId = @ManagerId

GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditPlayer]    Script Date: 2/25/2016 12:32:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditPlayer]
(
	@PlayerId int,
	@TeamId int,
	@PositionId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@JerseyNumber int,
	@RookieYear int,
	@LastSeasonBatAvg decimal(4,3)
) AS

UPDATE Players
	SET TeamId = @TeamId, PositionId = @PositionId, FirstName = @FirstName, LastName = @LastName,
		JerseyNumber = @JerseyNumber, RookieYear = @RookieYear, LastSeasonBatAvg = @LastSeasonBatAvg
WHERE PlayerId = @PlayerId

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditTeam]    Script Date: 2/26/2016 10:35:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditTeam]
(
	@TeamId int,
	@LeagueId int,
	@ManagerId int,
	@TeamName nvarchar(50)
) AS

UPDATE Teams
	SET LeagueId = @LeagueId, ManagerId = @ManagerId, TeamName = @TeamName
WHERE TeamId = @TeamId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[GetAManager]    Script Date: 2/26/2016 10:02:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAManager]
(
	@ManagerId int
) AS

SELECT * FROM Managers
WHERE ManagerId = @ManagerId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[GetManagers]    Script Date: 2/26/2016 10:03:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetManagers] AS

SELECT * FROM Managers
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllPlayers]    Script Date: 2/25/2016 12:32:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadAllPlayers] AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllTeams]    Script Date: 2/26/2016 10:35:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadAllTeams] AS

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName [Name]
FROM Teams t
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadManagerForTeam]    Script Date: 2/26/2016 10:34:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadManagerForTeam]
(
	@TeamId int
) AS

SELECT m.*
FROM Teams t
	INNER JOIN Managers m
	ON t.ManagerId = m.ManagerId
WHERE t.TeamId = @TeamId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPlayerById]    Script Date: 2/25/2016 12:32:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadPlayerById]
(
	@PlayerId int
) AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPlayersForTeam]    Script Date: 2/26/2016 10:34:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadPlayersForTeam]
(
	@TeamId int
) AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.TeamId = @TeamId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadTeamById]    Script Date: 2/26/2016 10:33:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadTeamById]
(
	@TeamId int
) AS

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName [Name]
FROM Teams t
WHERE t.TeamId = @TeamId
GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[RemovePlayer]    Script Date: 2/25/2016 12:32:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemovePlayer]
(
	@PlayerId int
) AS

DELETE
FROM Players
WHERE PlayerId = @PlayerId

GO

USE [BaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPositionForPlayer]    Script Date: 2/27/2016 6:07:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LoadPositionForPlayer]
(
	@PlayerId int
) AS

SELECT pos.*
	FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPositionForPlayer]    Script Date: 2/27/2016 6:07:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LoadPositionForPlayer]
(
	@PlayerId int
) AS

SELECT pos.*
	FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO



USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddAManager]    Script Date: 2/26/2016 10:02:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddAManager]
(
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@ManagerId int OUTPUT
) AS
INSERT INTO Managers (FirstName, LastName)
VALUES (@FirstName, @LastName);

SET @ManagerId = SCOPE_IDENTITY();

GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddPlayer]    Script Date: 2/25/2016 12:32:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddPlayer]
(
	@TeamId int,
	@PositionId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@JerseyNumber int,
	@RookieYear int,
	@LastSeasonBatAvg decimal(4,3)
) AS

DECLARE @id int

INSERT INTO Players (TeamId, PositionId, FirstName, LastName, JerseyNumber, RookieYear, LastSeasonBatAvg)
VALUES (@TeamId, @PositionId, @FirstName, @LastName, @JerseyNumber, @RookieYear, @LastSeasonBatAvg)

SET @id = SCOPE_IDENTITY()

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @id

--DECLARE @InsertedRecord table
--(	
--	PlayerId int,
--	TeamId int,
--	PositionId int,
--	FirstName nvarchar(50),
--	LastName nvarchar(50),
--	JerseyNumber int,
--	RookieYear int,
--	LastSeasonBatAvg decimal(4,3)
--)

--OUTPUT INSERTED.PlayerId, INSERTED.TeamId, INSERTED.PositionId, INSERTED.FirstName, INSERTED.LastName,
--	INSERTED.JerseyNumber, INSERTED.RookieYear, INSERTED.LastSeasonBatAvg
--	INTO @InsertedRecord
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddTeam]    Script Date: 2/26/2016 10:35:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddTeam]
(
	@LeagueId int,
	@ManagerId int,
	@TeamName nvarchar(50)
) AS 

DECLARE @id int

INSERT INTO Teams (LeagueId, ManagerId, TeamName)
VALUES (@LeagueId, @ManagerId, @TeamName)

SET @id = SCOPE_IDENTITY()

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName Name
FROM Teams t
WHERE t.TeamId = @id
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[DeleteAManager]    Script Date: 2/26/2016 10:02:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteAManager]
(
	@ManagerId int
) AS
DELETE FROM Managers
WHERE ManagerId = @ManagerId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditAManager]    Script Date: 2/26/2016 10:02:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EditAManager]
(
	@ManagerId int,
	@FirstName nvarchar(50),
	@Lastname nvarchar(50)
) AS
UPDATE Managers 
SET FirstName = @FirstName, LastName = @LastName
WHERE ManagerId = @ManagerId

GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditPlayer]    Script Date: 2/25/2016 12:32:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditPlayer]
(
	@PlayerId int,
	@TeamId int,
	@PositionId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@JerseyNumber int,
	@RookieYear int,
	@LastSeasonBatAvg decimal(4,3)
) AS

UPDATE Players
	SET TeamId = @TeamId, PositionId = @PositionId, FirstName = @FirstName, LastName = @LastName,
		JerseyNumber = @JerseyNumber, RookieYear = @RookieYear, LastSeasonBatAvg = @LastSeasonBatAvg
WHERE PlayerId = @PlayerId

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[EditTeam]    Script Date: 2/26/2016 10:35:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditTeam]
(
	@TeamId int,
	@LeagueId int,
	@ManagerId int,
	@TeamName nvarchar(50)
) AS

UPDATE Teams
	SET LeagueId = @LeagueId, ManagerId = @ManagerId, TeamName = @TeamName
WHERE TeamId = @TeamId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[GetAManager]    Script Date: 2/26/2016 10:02:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAManager]
(
	@ManagerId int
) AS

SELECT * FROM Managers
WHERE ManagerId = @ManagerId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[GetManagers]    Script Date: 2/26/2016 10:03:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetManagers] AS

SELECT * FROM Managers
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllPlayers]    Script Date: 2/25/2016 12:32:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadAllPlayers] AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadAllTeams]    Script Date: 2/26/2016 10:35:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadAllTeams] AS

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName [Name]
FROM Teams t
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadManagerForTeam]    Script Date: 2/26/2016 10:34:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadManagerForTeam]
(
	@TeamId int
) AS

SELECT m.*
FROM Teams t
	INNER JOIN Managers m
	ON t.ManagerId = m.ManagerId
WHERE t.TeamId = @TeamId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPlayerById]    Script Date: 2/25/2016 12:32:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadPlayerById]
(
	@PlayerId int
) AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.PlayerId = @PlayerId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPlayersForTeam]    Script Date: 2/26/2016 10:34:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadPlayersForTeam]
(
	@TeamId int
) AS

SELECT p.*, pos.PositionId, pos.Name [PositionName], pos.Abbreviation
FROM Players p
	INNER JOIN Positions pos
	ON p.PositionId = pos.PositionId
WHERE p.TeamId = @TeamId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadTeamById]    Script Date: 2/26/2016 10:33:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadTeamById]
(
	@TeamId int
) AS

SELECT t.TeamId, t.LeagueId, t.ManagerId, t.TeamName [Name]
FROM Teams t
WHERE t.TeamId = @TeamId
GO

USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[RemovePlayer]    Script Date: 2/25/2016 12:32:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemovePlayer]
(
	@PlayerId int
) AS

DELETE
FROM Players
WHERE PlayerId = @PlayerId

GO

