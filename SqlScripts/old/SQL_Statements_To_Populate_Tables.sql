USE TestBaseballLeague
GO

INSERT INTO Leagues (LeagueName) VALUES ('Ohio Baseball League')
INSERT INTO Managers (FirstName, LastName ) VALUES ('Lewis', 'Johnson')
INSERT INTO Managers (FirstName, LastName ) VALUES ('Ben', 'Larson')
INSERT INTO Managers (FirstName, LastName ) VALUES ('Bird', 'Hunter')
INSERT INTO Managers (FirstName, LastName ) VALUES ('Frank', 'Sampson')
INSERT INTO Managers (FirstName, LastName ) VALUES ('George', 'Wilson')
INSERT INTO Managers (FirstName, LastName ) VALUES ('Tom', 'Alvarez')
INSERT INTO Managers (FirstName, LastName ) VALUES ('Bill', 'Rodrigues')
INSERT INTO Managers (FirstName, LastName ) VALUES ('Tim', 'Lee')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 1, 'Braves')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 2, 'Kangaroos')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 3, 'Cardinals')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 4, 'Flyers')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 5, 'Cubs')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 6, 'Raiders')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 7, 'Angels')
INSERT INTO Teams (LeagueId, ManagerId, TeamName) VALUES (1, 8, 'Bucks')
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,1,'Alfaro', 'Jorge', 0, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,1,'Alfonzo', 'Eliezer', 2, 2014, 0.250)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,1,'Anderson', 'Bryan', 5, 2013, 0.315)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,1,'Arencibia', 'J.P.', 8, 2012, 0.630)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,1,'Ashley', 'Nevin', 10, 2010, 0.551)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,1,'Avila', 'Alex', 15, 2013, 0.470)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,1,'Baker', 'John', 20, 2014, 0.283)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,1,'Bandy', 'Jett', 30, 2015, 0.390)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,2,'Aardsma', 'David', 1, 2011, 0.665)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,2,'Abad', 'Fernando', 4, 2013, 0.510)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,2,'Acevedo', 'Andury', 7, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,2,'Aceves', 'Alfredo', 11, 2006, 0.130)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,2,'Achter', 'A.J.', 21, 2014, 0.328)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,2,'Acosta', 'Manny', 31, 2013, 0.650)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,2,'Adams', 'Austin', 42, 2012, 0.589)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,2,'Adams', 'Mike', 51, 2015, 0.470)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,3,'Abreu', 'Jose',  3, 2011, 0.146)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,3,'Adams', 'Matt',  6, 2013, 0.210)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,3,'Aguilar', 'Jesus',  9, 2014, 0.699)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,3,'Allen', 'Brandon',  13, 2010, 0.550)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,3,'Alonso', 'Yonder',  16, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,3,'Alvarez', 'Pedro',  22, 2011, 0.130)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,3,'Anderson', 'Lars',  33, 2013, 0.251)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,3,'Baker', 'Jeff',  44, 2015, 0.370)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,4,'Abreu', 'Tony',  1, 2014, 0.583)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,4,'Alcantara', 'Arismendy',  2, 2012, 0.490)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,4,'Altuve', 'Jose',  3, 2007, 0.165)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,4,'Amezaga', 'Alfredo',  24, 2011, 0.210)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,4,'Barney', 'Darwin',  35, 2013, 0.367)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,4,'Beckham', 'Tim',  46, 2014, 0.630)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,4,'Belliard', 'Ronnie',  57, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,4,'Blanco', 'Andres',  68, 2015, 0.450)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,5,'Adames', 'Cristhian', 5, 2010, 0.289)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,5,'Adrianza', 'Ehire', 6, 2011, 0.570)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,5,'Ahmed', 'Nick', 99, 2013, 0.346)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,5,'Alberto', 'Hanser', 82, 2014, 0.610)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,5,'Amarista', 'Alexi', 73, 2012, 0.199)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,5,'Andrus', 'Elvis', 45, 2008, 0.550)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,5,'Anna', 'Dean', 51, 2013, 0.315)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,5,'Arcia', 'Orlando', 63, 2015, 0.630)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,6,'Adams', 'David', 7, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,6,'Arenado', 'Nolan', 8, 2014, 0.470)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,6,'Arias', 'Joaquin', 9, 2013, 0.183)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,6,'Beckham', 'Gordon', 62, 2011, 0.290)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,6,'Bell', 'Josh', 51, 2010, 0.365)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,6,'Beltre', 'Adrian', 43, 2013, 0.610)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,6,'Brignac', 'Reid', 39, 2014, 0.567)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,6,'Bryant', 'Kris', 28, 2015, 0.430)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,7,'Ackley', 'Dustin', 17, 2012, 0.228)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,7,'Almonte', 'Zoilo', 2, 2013, 0.350)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,7,'Alou', 'Moises', 20, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,7,'Aoki', 'Nori', 45, 2011, 0.570)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,7,'Arcia', 'Oswaldo', 85, 2014, 0.446)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,7,'Asche', 'Cody', 65, 2013, 0.110)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,7,'Avery', 'Xavier', 35, 2009, 0.399)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,7,'Barnes', 'Brandon', 79, 2015, 0.650)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,8,'Adams', 'Lane', 74, 2011, 0.515)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,8,'Almonte', 'Abraham', 75, 2013, 0.430)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,8,'Altherr', 'Aaron', 78, 2014, 0.151)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,8,'Angle', 'Matt', 36, 2012, 0.270)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,8,'Benson', 'Joe', 35, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,8,'Bernard', 'Wynton', 32, 2013, 0.590)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,8,'Betts', 'Mookie', 52, 2010, 0.465)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,8,'Blackmon', 'Charlie', 58, 2015, 0.110)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (1,9,'Aldridge', 'Cory', 47, 2014, 0.267)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (2,9,'Alvarez', 'Dariel', 45, 2013, 0.330)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (3,9,'Aplin', 'Andrew', 42, 2011, 0.528)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (4,9,'Balentien', 'Wladimir', 21, 2012, 0.450)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (5,9,'Bautista', 'Jose', 25, 2013, 0.189)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (6,9,'Baxter', 'Mike', 23, 2014, 0.270)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (7,9,'Beltran', 'Carlos', 60, 2016, NULL)
INSERT INTO Players (TeamId, PositionId, LastName, FirstName, JerseyNumber, RookieYear, LastSeasonBatAvg) VALUES (8,9,'Beltre', 'Engel', 53, 2015, 0.610)
INSERT INTO Positions (Name, Abbreviation) VALUES ('Catcher', 'C')
INSERT INTO Positions (Name, Abbreviation) VALUES ('Pitcher', 'P')
INSERT INTO Positions (Name, Abbreviation) VALUES ('First Base', '1B')
INSERT INTO Positions (Name, Abbreviation) VALUES ('Second Base', '2B')
INSERT INTO Positions (Name, Abbreviation) VALUES ('Shortstop', 'SS')
INSERT INTO Positions (Name, Abbreviation) VALUES ('Third Base', '3B')
INSERT INTO Positions (Name, Abbreviation) VALUES ('Left Field', 'LF')
INSERT INTO Positions (Name, Abbreviation) VALUES ('Center Field', 'CF')
INSERT INTO Positions (Name, Abbreviation) VALUES ('Right Field', 'RF')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/1/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/1/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/1/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/2/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/2/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/2/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/3/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/3/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/3/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/8/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/8/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/8/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/9/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/9/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/9/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/10/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/10/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/10/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/15/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/15/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/15/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/16/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/16/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/16/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/17/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/17/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/17/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/22/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/22/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/22/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/23/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/23/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/23/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/24/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/24/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/24/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/29/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/29/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/29/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/30/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/30/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('4/30/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/1/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/1/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/1/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/6/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/6/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/6/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/7/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/7/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/7/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/8/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/8/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/8/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/13/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/13/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/13/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/14/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/14/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/14/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/15/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/15/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/15/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/20/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/20/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/20/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/21/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/21/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/21/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/22/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/22/2016')
INSERT INTO Games ([Date], HomeTeamId, AwayTeamId) VALUES ('5/22/2016')